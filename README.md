**Description**

If you think you're miserable for being stuck at home in pandemic doing chores like a robot, you're not alone. HamBoi, our little friend, is in a similar situation. Help HamBoi juggle through tasks to maintain his electricity, hunger and thirst level. Make sure he won't end up starving, crying and thirsty like us :crying_cat_face: 

**Controls**

Directional Buttons (W-A-S-D) / Arrow Keys : Move
E : Start an Activity
Escape / X Button : Quit an activity
Left Click, F, etc : Do Tasks
Click on the Sad Hamster : Reset Level

***Gameplay Trivia : ***
1. There are 3 main resources that depletes over time, and your main job is to make sure none of them reaches zero
2. Navigate through 4 Different tasks to regain resources.


*** Tasks ***
1. Hamster Wheel : Spam F to generate electricity at the expenses of your hunger
2. Hamster Food : Crack the sunflower seed, then right click to eat, MAKE SURE YOU BREAK THE SHELL BEFORE EATING!
3. Hamster Bottle : Catch the water droplets, restore thirst level.
4. Generator : Needed by Hamster Wheel to generate electricity, fix this otherwise hamster wheel wont work.


**Credits** 

1. Code : Andrew Halim (@conrev)
2. UI, Level Design : Andrew Halim, Evelyn Hendry (@luceve)
3. Music :  Mananda Hutagalung (@hiawata)
4. Art : Evelyn Hendry (@luceve)
5. SFX : from freesound.org

***Additional Credits:***

Cracking SFX from : https://freesound.org/people/JustInvoke/sounds/446118/ by JustInvoke

Game Engine : Godot 3.2.3

MUSIC : LMMS with VSCO2 Rompler
